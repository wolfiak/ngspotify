import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import {NavbarComponent} from './navbar/navbar.component';
import {AboutComponent} from './about/about.component';
import {SearchComponent} from './search/search.component';


const routes: Routes=[
    {
        path: '',
        component: SearchComponent
    },
    {
        path: 'about',
        component: AboutComponent
    }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}